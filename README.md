#说明：

> 此文件为Markdown格式  
> 在TextMate中可以用`Command+Option+Ctrl+P`来预览  
> 当然首选还是Markdown编辑器

##1. 建议下载TextMate来辅助学习，TextMate可以单文件编译运行，方便运行
###	步骤：
1. 下载安装TextMate(官网 http://textmate.org 或 http://soft.macx.cn/3172.htm)
2. 进入 Preferences > Bundles, 找到并勾上 Swift
3. 打开文件：用TextMate打开Swift文件（自动采用Swift拼写）
4. 写代码
5. 运行代码（`Command+R`），之后会出现窗口显示输出或错误

#### 注：
- 新建文档：`Command+Option+N`
- 修改拼写检查为Swift：`Shift+Ctrl+Option+S`，并选择Swift
- 若不使用TextMate，可使用Xcode编辑，再通过以下方式运行。

	1. 打开Terminal（终端）
	2. cd ~\Desktop\Exercise\
	3. xcrun swift filename.swift
		
##2. 我给的题目都是英文说明，个别词汇注中文。
##3. 需要你写的代码都用 "`// TODO`" 予以标注。
##4. 写好后可以自己运行来检查，然后有问题再问我，也可以都发回来给我看。
